package ir.setaremaghari.kotlinmvvm.model

/**
 * Created by Setare on 11/7/2017.
 */


data class Article(var title:String, var excerpt: String, var highlight: Boolean,
    var  imageUrl: String, var commentsNumber: Int, var read: Boolean = false
    )
