package ir.setaremaghari.kotlinmvvm.ui

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import ir.setaremaghari.kotlinmvvm.R
import ir.setaremaghari.kotlinmvvm.databinding.ArticleItemBinding
import ir.setaremaghari.kotlinmvvm.databinding.MainActivityBinding
import ir.setaremaghari.kotlinmvvm.model.Article
import ir.setaremaghari.kotlinmvvm.view_model.ArticleViewModel
import java.util.ArrayList

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val binding = DataBindingUtil.setContentView<MainActivityBinding>(this, R.layout.main_activity)
    val layoutManager = LinearLayoutManager(this)
    binding.contactList.layoutManager = layoutManager

    val articles = ArrayList<Article>()

    articles.add(Article("An outbreak of parasitic bees",
        "This summer, we are facing a very serious issue. And it is nothing else but an outbreak of parasitic bees.",
        true, "android.resource://ir.setaremaghari.kotlinmvvm/drawable/bee", 45))
    articles.add(Article("Brno - the city of 2016",
        "It has been announced by the committee of know-it-all that Brno has been elected city of year 2016.",
        false, "android.resource://ir.setaremaghari.kotlinmvvm/drawable/brno", 0))
    articles.add(Article("Restaurants in trouble",
        "Restaurants offering daily menus could soon face a serious trouble. The government has just...",
        true, "android.resource://ir.setaremaghari.kotlinmvvm/drawable/food", 1))
    articles.add(Article("Survey amongst drivers reveals shocking facts",
        "A survey taken by 1100 drivers commuting every day to work shows that the drivers mostly drive their car alone.",
        false, "android.resource://ir.setaremaghari.kotlinmvvm/drawable/driver", 33))
    articles.add(Article("Rugby for everyone?",
        "Until lately, rugby has been considered a sport played only by men. What are the consequences...",
        false, "android.resource://ir.setaremaghari.kotlinmvvm/drawable/rugby", 11))

    val adapter = ArticlesRecyclerAdapter(articles, this)
    /*
       * contactList is id of recycler view inside the xml layout
       * */
    binding.contactList.adapter = adapter

  }
}
