package ir.setaremaghari.kotlinmvvm.ui

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ir.setaremaghari.kotlinmvvm.R
import ir.setaremaghari.kotlinmvvm.databinding.ArticleItemBinding
import ir.setaremaghari.kotlinmvvm.model.Article
import ir.setaremaghari.kotlinmvvm.view_model.ArticleViewModel

/**
 * Created by Setare on 11/7/2017.
 */
class ArticlesRecyclerAdapter(var articles: List<Article>, var context: Context) : RecyclerView.Adapter<ArticlesRecyclerAdapter.BindingHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingHolder {

    /*
       * DataBinding creates XXBinding automatically
       * the XX comes from the name of related xml layout which has
       * <data> tag in it
       * */

    val binding = DataBindingUtil.inflate<ArticleItemBinding>(
        LayoutInflater.from(parent.context),
        R.layout.article_item, parent, false)

    return BindingHolder(binding)
  }

  override fun onBindViewHolder(holder: BindingHolder, position: Int) {
    /*
      * Avm is the name of <data> tag inside the xml layout that
      * you can name it anything you want
      * this method also is made automatically and you should pass
      * your ViewModel class to it
      * */
    val binding = holder.Hbinding
    binding.avm = ArticleViewModel(context,articles[position])

  }

  override fun getItemCount(): Int {
    return articles.size
  }

  class BindingHolder(binding: ArticleItemBinding) : RecyclerView.ViewHolder(
      binding.contactCard){
    /*
          * concatCard is the id of CardView inside the binding layout
          * you can access any item of the layout
          * */
    var Hbinding: ArticleItemBinding = binding

  }
}
