package ir.setaremaghari.kotlinmvvm.view_model

import android.content.Context
import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.BindingAdapter
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import ir.setaremaghari.kotlinmvvm.BR
import ir.setaremaghari.kotlinmvvm.R
import ir.setaremaghari.kotlinmvvm.model.Article

/**
 * Created by Setare on 11/7/2017.
 */
class ArticleViewModel(var context: Context,var articleModel: Article) : BaseObservable() {




  /*
  * In this class you can define your custom logic of the view
  * for ex you can define your custom getter and setter here
  *
  * */


  /*
  * @Bindable is used for getter of a variable which
  * has to act in observable way
  * it means that if the item changed anytime, the view
  * hast to refresh to
  * */
  @Bindable
  fun getTitle() = articleModel.title

  fun setTitle(title: String){
    articleModel.title = title
    notifyPropertyChanged(BR._all)
  }

  fun getCardBackgroundColor(): Int {
    return if (articleModel.highlight)
      ContextCompat.getColor(context, R.color.highlight)
    else
      Color.parseColor("#ffffffff")
  }


  fun getCommentsButtonVisibility(): Int {
   return when (articleModel.commentsNumber) {
      0 -> View.GONE
      else -> View.VISIBLE
    }
  }


  fun getCommentsNumber()= articleModel.commentsNumber


  fun getExcerpt() = articleModel.excerpt



  /*
  *you can define custom attributes inside your xml layout
  * for ex i have defined "app:setare" then i set imageUrl to it for my Imageview
  * it means that i want to have customize behavior for loading my image
  * so after defining a generam getter for ImageUrl
  * i can define a customize Adapter with @BindingAdapter for this
  * and for ex i can load my image with Glide instead
  * */

  fun getImageUrl() = articleModel.imageUrl

  /*
  * in method hatman bayad static bashe
  * joz companion bayad @JvmStatic ham balash gozashte she
  *
  * */
  companion object {

    @BindingAdapter("setare")
    @JvmStatic
    fun loadImageWithGlid(view: ImageView, url: String) {
      Glide.with(view.context).load(url).centerCrop().into(view)
    }
  }

  fun setRead(read: Boolean) {
    setTitle("READ: " + getTitle())
    articleModel.read = read
  }

  fun onReadMoreClicked(): View.OnClickListener {
    return View.OnClickListener { view ->
      Toast.makeText(view.context, "Opens article detail", Toast.LENGTH_SHORT).show()
      setRead(true)
    }
  }

  fun onCommentsClicked(): View.OnClickListener {
    return View.OnClickListener { view ->
      Toast.makeText(view.context, "Opens comments detail", Toast.LENGTH_SHORT).show()
    }
  }

}